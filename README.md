# README #

This repository hosts public test releases for [GBerry](http://gberry.xyz). 

GBerry is low cost game console platform supporting especially simultaneous multi-player gaming. The grand idea is bring social interaction of traditional board games into family living rooms in digital form.

You can find on this repository necessary binaries and instructions to try out GBerry on your own hardware. You need to have some experience from Raspberry Pi and Linux to be able to make the setup.

If you have questions or proposals please free to

* Go to [GBerry on Facebook](https://facebook.com/gberryproject) and post a message
* Email to project(at)gberry.xyz
* Post an issue or proposal to [issue tracker](https://bitbucket.org/theros/gberry/issues?status=new&status=open).


### What is this repository for? ###

This repository contains all binaries that are required to test GBerry game console platform.

* Console software that is run on Raspberry Pi
* Android mobile application


### How do I get set up? ###

See [wiki](https://bitbucket.org/theros/gberry/wiki) for details:

* [Hardware requirements](https://bitbucket.org/theros/gberry/wiki/Hardware%20Requirements)
* [Installation instructions](https://bitbucket.org/theros/gberry/wiki/Installation%20Instructions)
* [Running instructions](https://bitbucket.org/theros/gberry/wiki/Running%20Instructions)


### Contribution guidelines ###

GBerry game platform software and mobile application will be released as open source. Stay tuned!